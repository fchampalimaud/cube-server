# 3D Cube server

Rotate a 3D cube based on remote network commands. 

Developed with Python 2.7.

Cube rendering based on the following examples:

* https://github.com/petercollingridge/Pygame3D
* http://www.petercollingridge.co.uk/book/export/html/460
* http://codentronix.com/2011/04/21/rotating-3d-wireframe-cube-with-python/
* http://stackoverflow.com/questions/30745703/rotating-a-cube-using-quaternions-in-pyopengl

![Screenshot](https://bytebucket.org/fchampalimaud/cube-server/raw/e17f0ad05a6bc0be143ead6103bb5055830b4304/screenshot.png)

## Requirements

You will need PyGame. 

Installation in OSX (python3 is optional):
	
	brew instal pygame --with-python3

## How to run?

Inside project folder run this command:

python -m cubeserver

By default, the server listens on localhost:9999.For broadcast, please refer to this link: http://stackoverflow.com/questions/19032609/broadcasting-socket-server-in-python.

## How to control the cube remotely?

Use test-client.py to try it. It is a UDP client script.

Available actions:

* 'GO_LEFT' : 1,
* 'GO_RIGHT' : 2,
* 'GO_UP' : 3,
* 'GO_DOWN' : 4,
* 'FALL_LEFT': 5,
* 'FALL_RIGHT': 6

Example:

	python test-client.py 2

Expected result: the cube will turn right.