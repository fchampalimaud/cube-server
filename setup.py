#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('cubeserver/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')


setup(
    name='cube-server',
    version=version,
    description="""3D Cube Server""",
    author='Carlos Mão de Ferro',
    author_email='cajomferro@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/fchampalimaud/cube-server',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

#    install_requires=[
#        'pycontrol-api >= 0.7.3',
#        'pyforms >= 0.1.6',
#        'Send2Trash >= 1.3.0'
#    ],

    entry_points={
        'gui_scripts': [
            'cube-server=cuberserver.__main__',
        ],
    }
)
