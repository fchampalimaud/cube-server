#!/bin/env python

import wireframe3 as wireframe
import pygame

label_to_action = {
    'GO_LEFT' : 0,
    'GO_RIGHT' : 1,
    'GO_UP' : 2,
    'GO_DOWN' : 3,
    'FALL_LEFT': 4,
    'FALL_RIGHT': 5
}

# class CubeServerEvent(object):
#     def __init__(self, cube):
#         self.cube = cube

#     def apply(self,):
#         pass

# class CubeServerEventLeft(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('Y',  -0.1)

# class CubeServerEventRight(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('Y',  0.1)

# class CubeServerEventUp(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('X',  -0.1)

# class CubeServerEventDown(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('X',  0.1)

# class CubeServerEventFallLeft(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('Z',  -0.1)

# class CubeServerEventFallRight(CubeServerEvent):

#     def apply(self):
#         self.cube.rotateAll('Z',  0.1)


class ProjectionViewer:
    """ Displays 3D objects on a Pygame screen """

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Wireframe Display')
        self.background = (10,10,50)

        self.wireframes = {}
        self.displayNodes = True
        self.displayEdges = True
        self.displayFaces = False
        self.nodeColour = (255,255,255)
        self.edgeColour = (200,200,200)
        self.faceColour = (50,200,250)
        self.nodeRadius = 4

        self.clock = pygame.time.Clock()

        self.server_events = []


    def addWireframe(self, name, wireframe):
        """ Add a named wireframe object. """

        self.wireframes[name] = wireframe

    def update(self, next_action):
        """ Update cube position. """

        if next_action == label_to_action['GO_LEFT']:
            self.rotateAll45('Y',  -0.1)
        if next_action == label_to_action['GO_RIGHT']:
            self.rotateAll45('Y',  0.1)
        if next_action == label_to_action['GO_UP']:
            self.rotateAll45('X',  0.1)
        if next_action == label_to_action['GO_DOWN']:
            self.rotateAll45('X',  -0.1)
        if next_action == label_to_action['FALL_LEFT']:
            self.rotateAll45('Z',  -0.1)
        if next_action == label_to_action['FALL_RIGHT']:
            self.rotateAll45('Z',  0.1)

      
    def display(self):
        """ Draw the wireframes on the screen. """

        self.screen.fill(self.background)

        for wireframe in self.wireframes.values():
            if self.displayEdges:
                for edge in wireframe.edges:
                    pygame.draw.aaline(self.screen, self.edgeColour, (edge.start.x, edge.start.y), (edge.stop.x, edge.stop.y), 1)

            if self.displayNodes:
                for node in wireframe.nodes:
                    pygame.draw.circle(self.screen, self.nodeColour, (int(node.x), int(node.y)), self.nodeRadius, 0)

            if self.displayFaces:

                # Calculate the average Z values of each face.
                avg_z = []
                i = 0
                for f in self.faces:
                    z = (t[f[0]].z + t[f[1]].z + t[f[2]].z + t[f[3]].z) / 4.0
                    avg_z.append([i,z])
                    i = i + 1

                for tmp in sorted(avg_z,key=itemgetter(1),reverse=True):
                    face_index = tmp[0]
                    f = self.faces[face_index]
                    pointlist = [(t[f[0]].x, t[f[0]].y), (t[f[1]].x, t[f[1]].y),
                                 (t[f[1]].x, t[f[1]].y), (t[f[2]].x, t[f[2]].y),
                                 (t[f[2]].x, t[f[2]].y), (t[f[3]].x, t[f[3]].y),
                                 (t[f[3]].x, t[f[3]].y), (t[f[0]].x, t[f[0]].y)]
                    pygame.draw.polygon(self.screen,self.colors[face_index],pointlist)

    def run(self):
        """ Create a pygame screen until it is closed. """

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    
            self.display()  
            pygame.display.flip()                    


    def translateAll(self, axis, d):
        """ Translate all wireframes along a given axis by d units. """

        for wireframe in self.wireframes.itervalues():
            wireframe.translate(axis, d)

    def scaleAll(self, scale):
        """ Scale all wireframes by a given scale, centred on the centre of the screen. """

        centre_x = self.width/2
        centre_y = self.height/2

        for wireframe in self.wireframes.itervalues():
            wireframe.scale((centre_x, centre_y), scale)

    def rotateAll(self, axis, theta):
        """ Rotate all wireframe about their centre, along a given axis by a given angle. """

        rotateFunction = 'rotate' + axis

        for wireframe in self.wireframes.itervalues():
            centre = wireframe.findCentre()
            getattr(wireframe, rotateFunction)(centre, theta)

    def rotateAll45(self, axis, theta):
        """ Rotate all wireframe about their centre, along a given axis by a given angle. """

        rotateFunction = 'rotate' + axis

        for i in range(1, 17):            
            for wireframe in self.wireframes.itervalues():
                centre = wireframe.findCentre()
                getattr(wireframe, rotateFunction)(centre, theta)

            self.clock.tick(20)
            self.display()
            pygame.display.flip()


if __name__ == '__main__':
    pv = ProjectionViewer(400, 300)

    cube = wireframe.Wireframe()
    cube.addNodes([(x,y,z) for x in (50,250) for y in (50,250) for z in (50,250)])
    cube.addEdges([(n,n+4) for n in range(0,4)]+[(n,n+1) for n in range(0,8,2)]+[(n,n+2) for n in (0,1,4,5)])
    
    pv.addWireframe('cube', cube)

    # running = True
    # while running:
    #     for event in pygame.event.get():
    #         if event.type == pygame.QUIT:
    #             running = False
                
    pv.display()  
    pygame.display.flip() 

    actions = [0, 1, 2, 3, 1, 0 ,4, 5]
    next_action = 0

    running = True
    while(running):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_n:
                    pv.update(actions[next_action])
                    next_action += 1
                    if next_action == len(actions):
                        next_action = 0